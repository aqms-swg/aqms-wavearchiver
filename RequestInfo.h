/** @file                                                                                                            
* @ingroup group_wavearchiver                                                                                        
* @brief Header file for RequestInfo.C
*/
/***********************************************************/
/*
 * File Name :
 *	RequestInfo.h
 *
 * Original Author:
 *	Patrick Small
 *
 * Description:
 *
 *
 * Creation Date:
 *	14 June 1999
 *
 * Modification History:
 *	03 Mar 2017 - ADG: Updated for 64-bit system compliance	
 *
 * Usage Notes:
 */
/**********************************************************/

#ifndef request_info_H
#define request_info_H

// Various include files
#include <stdint.h>
#include "DatabaseLimits.h"
#include "TimeStamp.h"

/// Encapsulates a request being processed
class RequestInfo
{
 private:

 protected:

 public:
    int evidind; 
    uint32_t evid; /**< event id */
    int src_evidind;
    char src_evid[DB_MAX_LOCEVID_LEN+1];

    //Variables for incremental updates//
    uint32_t rcid; /**< request card id */
    uint32_t wfid; /**< waveform id */
    int pcstored; /**< percentage of waveform stored */
    int pcretrieved; /**< percentage of request  retrieved */
    int retry;       /**< number of times request card has been attempted */
    TimeStamp arrival_time; 

    RequestInfo(); 
    RequestInfo(const RequestInfo &r);
    ~RequestInfo();

    /// Compare request cards.
    RequestInfo& operator=(const RequestInfo &r);
    friend int operator<(const RequestInfo &w1, const RequestInfo &w2);
};


#endif
