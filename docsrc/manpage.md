# wavearchiver {#man-wavearchiver}
Waveform archiver

## PAGE LAST UPDATED ON
2020-02-19

## NAME
wavearchiver - Process that archives waveform data in miniseed based on requests in request card table.

## VERSION and STATUS
v3.1.5 2018-10-11
status : ACTIVE

## PURPOSE
Waveform archiver or wave archiver for short is used to retrieve waveform data, in miniSEED, from the wave servers and write them to the local machine. Its main purpose is to fulfill request cards stored in the request card table. Once the waveform is retrieved from the servers the associated request card is deleted. The wave archiver is a multi-threaded process that runs continuously in the background. It supports both Oracle and Postgres databases.

## HOW TO RUN
```
usage: ./wa <config file>

Version v3.1.3 2016-08-05
```
## CONFIGURATION FILE
The configuration file for the wave archiver is typically name wa.cfg and is described below. Format is  
```
<parameter name> <data type> <default value>  
<description>
```   

**General configuration parameters**  

**LogFile** *string*  
Full path to the log file. The wave archiver rotates the log file on a daily basis.
            
**LoggingLevel** *number*  
Number representing the level of logging. 0=ERROR only, 1=ERROR+INFO, 2=ERROR+INFO+DEBUG, in any such in source
            
**ProgramName** *string*  
Identifies the program instance. It must be different for each instance of the program running on the same machine.
            
**Application configuration parameters**  

**RequestType** *string*  
Type of request. Valid options are "Continuous" or "Triggered"
            
**OutputDir** *string*  
The path of the base directory in which to place the output miniSEED files.
    
**Network** *string*  
Network ID of the operating network.

**Source** *string*  
Source ID of the waveforms.
            
**RequestSource** *string*  
Set RequestSource only if you want the wavearchiver to process a specific request_card.subsource where the
the jiggle.jar RequestGenerator sets request_card.subsource to the value of its "subsource" property, 
which if not specified, defaults to RCG). For example, PowerLaw_RCG_module.props subsource=rcg_rt   
            
**MaxValidTime** *number*  
The maximum number of days that a request card is considered valid.

**PollInterval** *number*
The interval of time, in seconds, to wait between polls of the database for new request cards.
            
**WaveformPercentageThreshold** *number*    
Percentage of the requested time period that the retrieved waveform must meet to be considered "complete". 
Once the waveform request is complete, that request card will be deleted. Otherwise, wa will continue to try 
the request until the request is completed or the request exceeds MaxValidTime.

**Order** *string*  
The order in which request cards should be processed. Only two orders are allowed.
Oldest : FIFO method is used to process the request cards.  
Newest : LIFO method is used to process the request cards.
            
**MaxRequests** *number*  
The number of request cards to read from the database table at a time.
    
**Database configuration**  
    
**Include** *string*  
File containing database username, password, connection string, etc.
            
*Below values override those set, if any, in above file. 
DBConnectionRetryInterval and DBMaxConnectionRetries are used in loop when threads have no db connection available for query*

**DBService** *string*    
Database to connect to  

**DBUser** *string*  
Database user name

**DBPasswd** *string*  
Database password  

**DBConnectionRetryInterval** *number*  
Time to wait, in seconds, before re-attempting connection to the database.
    
**DBMaxConnectionRetries** *number*  
Max number of times to attempt database connection.
    
**Wave server configuration**
    
**WaveServerConfig** *string*  
Name of file containing wave server configuration. 
    
**StorageThreads** *number*  
Storage threads write mseed files to disk and update waveform related database table rows
they pop the queue filled by the archiver threads processing requests for channel timeseries
from the waveservers

**RetryMin** *number*  
RetryMin is minimum request_card.retry value to process, defaults to 0 when not declared.
You could set value >0 for a second wavearchiver to process higher retry (older) request values
when a primary wavearchiver is configured to with a RetryMax value
    
**RetryMax** *number*  
See note above about RetryMin property, wavearchiver ignores requests whose retry value is greater
than RetryMax. Set to small value if you have a second archiver for older higher retry requests,
or you want to ignore requests after max retries, at which point it skips doing a retry of once per day
(see RetryPowerBaseSecs property below).

**RetryPowerBaseSecs** *number*  
Requests satisfy query when current time is >= the lesser of last_``retry_time + RetryPowerBaseSecs**(retry+1) secs
or a 1 day (86400 s) interval``. Default = 4, so retry intervals are 16, 64, 256 ... up to 86400 seconds.
Previously, retry interval hard-coded as 10**retry, i.e.: 10, 100, 1000, 10000 ... up to 86400 seconds.

**RetryPriorityWgt** *number*  
It plays into the priority calculation for request cards. 
If a weight is provided, priority = request_card.priority - wgt * request_card.retry.
            
**MaxPacketLatencySecs** *number*  
This is the longest time needed to fill a packet for the slowest one 
or at least the vast majority of channels. It plays into the wait time calculation for retries.  

*A sample configuration is shown below*
```
#
# General configuration parameters
#
Logfile         ./wa.log
LoggingLevel    2
ProgramName     Wave_Archiver

#
# Application configuration parameters
#
RequestType                     Triggered
OutputDir                       ./outfiles
Network                         CI
Source                          SCDC
MaxValidTime                    7
PollInterval                    60
WaveformPercentageThreshold     100
Order                           Oldest
MaxRequests                     128
DBService                       somedb
DBUser                          someuser
DBPasswd                        somepassword
DBConnectionRetryInterval       5
DBMaxConnectionRetries          5
WaveServerConfig                ./waveservers.cfg
#
RetryMin                    0
RetryMax                  999
RetryPowerBaseSecs         10
RetryPriorityWgt            0
MaxPacketLatencySecs       10
#
```


*The parameter WaveServerConfig above contains wave server configurations and is described below.* 
```    
#The configuration file has two main sections:  
#LoadBalancingGroups  
#Archivers    
  
      
#LoadBalancingGroups defines groups of wave servers as shown below.  
#Each group has a "group_name" which is an arbitrary string.   
#Each group will have the parameters "servers", "threads" and "queue_size".   
#Servers are described by the parameters :  
#"host" is either an IP address or FQDN of the wave server  
#"port" is the well known port number the wave server listens on
#"load" controls the number of threads to be associated with each server   
#For e.g. for host1, load: 50 means 50% of "threads" will be associated with host1  
#Servers will be accessed in the order they are listed under "servers".  
#      
#"threads" are the number of threads associated with each server group.   
#  
#Each server group has an associated queue, the size of which is indicated by "queue_size".   
#This is where request cards retrieved from the database are queued for processing.   
#If a queue is full, request cards will not be added to this queue until space on the queue becomes available again.  

LoadBalancingGroups : 

    (
    {
        group_name : "cit" ;
        servers : 
           (
                { host: "host1.some.domain.edu";
                  port: 6509;
                  load: 50;
                },
                { host: "host2.some.domain.edu";
                  port: 6509;
                  load: 50;
                }
           );

        threads: 20;
        queue_size: 200;
    },

    {
        group_name: "nc" ;
        servers : 
        (
                {
                  host: "xyz";
                  port : 1133;
                  load: 50;
                },
                {
                  host: "abc";
                  port: 1234;
                  load: 50;
                }
        );

        threads: 2;
        queue_size: 50;
    }

    );

#Archivers section allows to direct request cards to appropriate servers based on filters. 
#"selectors" are lists whose values can be default, net codes or station codes. 
#"order" is a list of server groups to use for it's selector
    
    Archivers :
    (
        {
                selectors: ("default");
                order:  ( "cit" );
        },

        {
                selectors: ("BK","NC");
                order: ("nc", cit");
        }

    );
```

## DEPENDENCIES
     
*  aqms-libs.
*  Oracle or Postgres client software.
*  %Database tables "waveform", "request_card", "filename", "assocwae" and "assoc_wvday" must be available.
*  Disk space must be available to store large amounts of waveforms.
*  Wave servers must be running to accept request from the wave archiver.
*  System Time must be sync'ed using NTP since the request card SQL is using system time to find out how far to go back in time to retrieve them.
*  conlog. Conlog is a binary that logs the messages displayed on the console to a log file. While the wave archiver can run without conlog, it is a very helpful log file to have.
    
## MAINTENANCE

Make sure it keeps running 24/7 and disk space and database are available all the time.

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-wavearchiver/issues
