# Wave Archiver {#wavearchiver}  
  
Waveform archiver or wave archiver (wa) for short is used to retrieve waveform data, in miniSEED, from the wave servers and write them to a configured location. It is a multi-threaded process that runs continuously in the background. It supports both Oracle and Postgres databases.       
Its main purpose is to fulfill request cards stored in the request card table. Once the waveform is retrieved from the servers the associated request card is deleted. The *wa* is retry unfulfilled or partially fulfilled requests. Please see "Wave archiver tenacity" for more details regading retries.  
  
It is valid to run multiple instances of *wa* provided they do not attempt to process the same request cards.   
  
  
A flow chart of the logic is shown below.


<img src="https://gitlab.com/aqms-swg/aqms-docs/-/raw/master/docsrc/images/wa.gif" />
  
<br>  
  
### Wave archiver tenacity  
  
If *wa* is unable to fulfill a request it will retry at intervals according to a retry schedule. Data from stations can come into a seismic network at different times depending on the health of the stations. The retries help the wavearchiver get as much data as possible for it's request cards. The retry schedule is controlled by 3 configuration parameters that work together.    
  
*  **RetryPowerBaseSecs** : Requests satisfy query when current time is >= the lesser of last_retry_time + RetryPowerBaseSecs**(retry+1) secs or a 1 day (86400 s) interval. Default = 4, so retry intervals are 16, 64, 256 ... up to 86400 seconds. Previously, retry interval hard-coded as 10**retry, i.e.: 10, 100, 1000, 10000 ... up to 86400 seconds.  
*  **RetryPriorityWgt** : It plays into the priority calculation for request cards. If a weight is provided, priority = request_card.priority - wgt * request_card.retry.  
*  **MaxPacketLatencySecs** : This is the longest time needed to fill a packet for the slowest one or at least the vast majority of channels. It plays into the wait time calculation for retries.   

Some additional retry configuration parameters  

*  **RetryMin** : RetryMin is minimum request_card.retry value to process, defaults to 0 when not declared. You could set value >0 for a second wavearchiver to process higher retry (older) request values when a primary wavearchiver is configured to with a RetryMax value  
*  **RetryMax** : See note above about RetryMin property, wavearchiver ignores requests whose retry value is greater than RetryMax. Set to small value if you have a second archiver for older higher retry requests, or you want to ignore requests after max retries, at which point it skips doing a retry of once per day (see RetryPowerBaseSecs property below).    
  
The following columns in the RequestCard table are used to support the *wa* track retries.  
  
*  *retry* is the count of attempts to retrieve this waveform  
*  *lastretry* is the time of last attempt to retrieve this waveform   
*  *priority* means rows are processed in priority order, 1 is highest priority 
  

The following columns present in the RequestCard table support *wa* bookkeeping for partially fulfilled requests. When only part of the request time window is retrieved it will be saved (something is better than nothing) but the wa will continue to poll waveservers for the full time window.  
  
*  *PcStored* is the percent of the waveform that has been stored.    
*  *WFID* is the ID of the partial waveform stored.  
  

### Load balancing  
  
As mentioned, *wa* is a multithreaded process that can get data from one or more waveservers. There is a need for load balancing so that any particular wave server does not get overloaded. An example of how this is done is given below.  
  
*From email from Kalpesh Solanki 2/13/2008* 
   
```
Lets say there are four wave servers A,B,C,D  and "number of threads per sever" (numthreads) is 5.
so when i = 0, there will be 4 waveclient instances each having 4 servers in its list.
as follows: 

(i = 0)
thread 1: A B C D
thread 2: B A C D
thread 3: C A B D
thread 4: D A B C

I choose to have this pattern, because wave archiver client code first tries to connect to the first wave server in the list and then
second..so on and so forth.
so A don't get hits for all the requests but gets distributed among all servers , A B C D. Now as we have numthreads set to 5, there will
be 4x5 = 20 threads working to process the request cards.
```
  

### Request card generation  
  
A request card is a row in the request_card table containing a channel and time window information. See [Request card generator (RCG)](rcg.html).  

 

  







