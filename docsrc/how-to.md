How to compile and install the wavearchiver {#how-to-compile-install-wa}
===========================================

# Pre-requisites

wavearchiver depends on aqms-libs, so ensure that they are compiled and available before compiling wa

# Compile

For g++, use
make -f Makefile.g++ 

else, use
make -f Makefile

This will create a binary named wa.

# Install

On your target machine, create a folder named wavearchiver.
Copy the binary *wa* and the sample config files *wa.cfg* and *waveserver.cfg* to this folder. 
Modify the config files as needed.

Install the compiled aqms-libs and add them LD_LIBRARY_PATH environment variable.

In addition to logging to the log file, the wave archiver writes to the console. In order to capture these console messages, you can use the binary conlog. Please see the repo aqms-tools for more details.


# Configure

Modify the sample wa.cfg and waveserver.cfg as per your requirements.

Ensure that Logfile is available and writable to by the wave archiver.

Ensure that the wave servers mentioned in waveserver.cfg are running and reachable. The wave archiver will terminate if it cannot reach any server in the config.

Ensure that OutputDir in wa.cfg is available and writable by the wa binary.

Ensure that the database is running and reachable by the wa.