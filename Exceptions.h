/** @file                                                                                                            
* @ingroup group_wavearchiver                                                                                        
* @brief Header file for Exceptions.C                                                                                     
*/

#ifndef __exceptions_h
#define __exceptions_h
#include <list>
#include <string>

using namespace std;

typedef  list<char*> StringList;

class Error{

    StringList msgstack;

 public:
    Error();
    Error(const char*);
    Error(string);
    ~Error();

    void addMessage(char*);
    void print(void);
};


class LogicalError : public Error{
 public:
    LogicalError();
    LogicalError(char* str);
};

class CriticalError : public Error{
 public:
    CriticalError();
    CriticalError(char* str);
};

class OutofMemoryError : public CriticalError{
 public:
    OutofMemoryError();
};

class DatabaseError : public LogicalError{

 public:
    DatabaseError();
    DatabaseError(char* str);
};

class FileFormatError : public LogicalError{
 public:
    FileFormatError();
    FileFormatError(char* str);
};

class FileIOError : public LogicalError{
 public:
    FileIOError();
    FileIOError(char* str);
};


#endif
