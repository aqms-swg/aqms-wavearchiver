/** @file                                                                                          
* @ingroup group_wavearchiver                                                                     
* @brief Header file for DatabaseWA.C                                                                        
*/
/***********************************************************/
/*
*
* File Name :
*        DatabaseWA.h
*
* Original Author:
*        Patrick Small
*
* Description:
*
*
* Creation Date:
*        05 May 1999
*
* Modification History:
*
*
* Usage Notes:
*/
/**********************************************************/

#ifndef database_wa_H
#define database_wa_H

#define OTL_STREAM_POOLING_ON

/// Various include files
#include <map>
#include <vector>
#include "Database.h"
#include "RequestCard.h"
#include "RequestInfo.h"
#include "WaveformArchive.h"

using namespace std;

/// Definition of the request card list
typedef std::multimap<RequestCard, RequestInfo, std::less<RequestCard>, 
    std::allocator<std::pair<const RequestCard, RequestInfo> > > RequestList;


/// Definition of the waveform archive list
//typedef std::multimap<RequestInfo, WaveformArchive, std::less<RequestInfo>, 
//   std::allocator<std::pair<const RequestInfo, WaveformArchive> > > WaveformList;

typedef  multimap<RequestInfo*,WaveformArchive> WaveformList;

/// Definition of the possible retrieval orders
typedef enum {ORDER_OLDEST, ORDER_NEWEST, ORDER_NOPRIORITY_OLDEST, ORDER_NOPRIORITY_NEWEST} retrievalOrder;

/// Database class for wave archiver specific tasks
class DatabaseWA : public Database
{
 private:
    retrievalOrder order;

    char requestSrc[32];
    char dbservice[MAXSTR];

    int maxrequests;
    int maxPacketLatencySecs;

    int _WriteFilenames(vector<int> &fileids, WaveformList &wl);
    int _WriteWaveforms(const char *network, const char *source,
			vector<int> &wfids, vector<int> &fileids,
			WaveformList &wl);
    int _WriteAssocWAE(vector<int> &wfids, WaveformList &wl);
    int _WriteAssocWV(vector<int> &wfids, WaveformList &wl);
    int _WriteTriggered(const char *network, const char *source, 
		       WaveformList &wl);
    int _WriteContinuous(const char *network, const char *source, 
			WaveformList &wl);

 protected:

 public:
    DatabaseWA(); 
    DatabaseWA(const char *dbs, const char *dbu, const char *dbp); /**< Constructor to make database connection */ 
    ~DatabaseWA(); 

    int SetRetrievalOrder(retrievalOrder ro); /**< Set retrieval order of request cards */
    int SetRequestSource(char *rsrc); /**< Set source ID of request cards */
    int SetMaxNewRequests(int num); /**< Set number of new request cards that can be read simultaneiously */
    int SetMaxPacketLatencySecs(int num); /**< Set longest time needed to fill a packet for the slowest one or at least the vast majority of channels  */

    /// Increments the retry count for a list of request cards
    int IncrementRetries(RequestList &rl); 

    /// Delete the list of request cards from the database
    int DeleteRequests(RequestList &rl);

    /// Write a list of waveform archives to the database
    int WriteWaveforms(const char *network, const char *source, 
		       WaveformList &wl);

    /// Check for requests cards that are older than "days" days
    /// and deletes them from the database. The deleted request cards
    /// are returned.
    int CheckOldRequests(int days, requestType rtype, RequestList &rl);

    /// Get all requests of a certain type that are newer than "days" days
    int GetRequestsNewer(RequestList &rl, int days, requestType rtype, int retryBase, double retryWt, int retryMin, int retryMax);

    // New functions for incremental updates
    int _UpdateRequestCards(WaveformList &wl);
    int _UpdateWaveforms(WaveformList &wl);
    int _UpdateFilenames(WaveformList &wl);

    /// Update STATUS in request card table
    int UpdateRequestCardsStatus(RequestList& rl,string status);
    int ResetRequestCardStatus(void);
    int getPendingRCCount(int& rccount, requestType rtype);
    char* getDbService(void);
};


#endif
