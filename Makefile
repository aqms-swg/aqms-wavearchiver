########################################################################
#
# Makefile     : Wave Archiver Program
# Author       : Patrick Small
#
# Last Revised : June 17, 1999
#
########################################################################

include $(DEVROOT)/shared/makefiles/Make.includes

BIN	= wa

#LIBCONFIG =  

#INCL    = $(RTSTDINCL) -I$(LIBCONFIG)
INCL    = $(RTSTDINCL)

LIBS	= $(RTLIBPATH)  -ltnwaveclient    \
    -ltnframework_nocms -lrtevent \
    -ltntcp -ltnwave -ltnchnl -ltndb  -ltntime -ltnstd -lpthread \
     $(SVR4LIBS)  $(QLIB2LIBS) $(ODBLIBS) -lconfig++


#libs converted to g++ : tnwaveclient, tnframework_nocms, rtevent, tntcp,
# tnchnl, tndb, tntime, tnstd tnwave

#CFLAGS 	+= -mt

CFLAGS  +=  -D_REENTRANT



OBJS = Main.o WaveArchiver.o RequestInfo.o DatabaseWA.o WaveArchiverPropertiesST.o Exceptions.o

all:${BIN}

wa:${OBJS}
	${CC}  $(CFLAGS) ${OBJS} -o $@ ${LIBS}


.C.o: 
	$(CC) $< -c -g $(CFLAGS) $(INCL)

.cc.o: 
	$(CC) $< -c -g $(CFLAGS) $(INCL)


clean:
	-rm -f *.o *~ core ${BIN} 
	-rm -rf SunWS_cache 
