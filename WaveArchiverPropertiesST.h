/** @file
 * @ingroup group_wavearchiver
 * @brief Header file for WavearchiverPropertiesST.cc
 */
/*
  Author: Kalpesh Solanki
 */

#ifndef __WaveArchiverPropertiesST_h
#define __WaveArchiverPropertiesST_h
#include <stdio.h>
#include <cstring>
#include <cstdlib>
#include <map>
#include <fstream>
#include "RetCodes.h"
#include "Exceptions.h"

using namespace std;

/// type definition of map containing configuration keys and values.
typedef map<string,string> KeyValueMap;


/// Encapsulates wavearchiver configurations.
class WaveArchiverPropertiesST{
 private:

             string _logfile;
	     int _logginglevel;
	     string _programname;
	     string _archivename;
	     string _requesttype;
	     string _outputdir;
	     string _requestSrc;
	     string _network;
	     string _source;
	     int _maxvalidtime;
	     int _pollinterval;
	     string _order;
	     int _maxrequests;
	     int _maxPacketLatencySecs;
	     string _dbservice;
	     string _dbuser;
	     string _dbpasswd;
	     string _waveserverconfig;
	     int _waveformpercentagethreshold;

	     int _storagethreads;
             int _retryMin;
             int _retryMax;
             int _retryPowerBaseSecs;
             double _retryPriorityWgt;
             int _dbConnRetryInterval;
             int _dbConnMaxRetries;
	  

    	KeyValueMap kvm;

	static WaveArchiverPropertiesST *handle;
        int checkPropertyKey(char* key);
	int toInt(string) throw(Error);
	float toFloat(string) throw(Error);
	double toDouble(string) throw(Error);
	bool toBool(string) throw(Error);

	WaveArchiverPropertiesST();

 public:

	static WaveArchiverPropertiesST* getInstance();

	int init(string) throw(Error);

	string getLogfile() const;
	int getLoggingLevel() const;
	int getDBMaxConnectionRetries() const ; 
	int getDBConnectionRetryInterval() const; 
	string getProgramName() const;
	string getRequestType() const;
	string getRequestSrc() const;
	string getNetwork() const;
	string getSource() const;
	int getMaxValidTime() const;
	int getPollInterval() const;
	string getOrder() const;
	string getOutputDir() const;
	string getArchiveName() const;
	string getRequestSource() const;
	int getMaxRequests() const;
	int getMaxPacketLatencySecs() const;
	string getDBService() const;
	string getDBUser() const;
	string getDBPasswd() const;
	string getWaveServerConfig() const;
	int getWaveformPercentageThreshold() const;
	int getStorageThreads() const;
	int getRetryMin() const;
	int getRetryMax() const;
	int getRetryPowerBaseSecs() const;
	double getRetryPriorityWgt() const;
	
};

#endif
