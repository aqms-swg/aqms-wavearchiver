/** @file
 * @ingroup group_wavearchiver
 * @brief Read and process wavearchiver config file
 */
/*
*  Author: Kalpesh Solanki
*/

#include <stdio.h>
#include <errno.h>
#include <iostream>
#include "WaveArchiverPropertiesST.h"
#include "Configuration.h"
#include "GenLimits.h"

WaveArchiverPropertiesST * WaveArchiverPropertiesST::handle = NULL;



//! Constructor.
WaveArchiverPropertiesST::WaveArchiverPropertiesST(){};


//! Create new or return existing instance of class. 
WaveArchiverPropertiesST* WaveArchiverPropertiesST::getInstance(){
  if(handle==NULL){
    handle = new WaveArchiverPropertiesST();
  }
  return handle;
}


//! Check if key, value pairs in configuration file are valid.
int WaveArchiverPropertiesST::checkPropertyKey( char* key ) {
   	if (strcmp("Logfile", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("LoggingLevel", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("ProgramName", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("RequestType", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("OutputDir", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("ArchiveName", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("RequestSource", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("Network", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("Source", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("MaxValidTime", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("PollInterval", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("Order", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("MaxPacketLatencySecs", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("RetryMin", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("RetryMax", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("RetryPowerBaseSecs", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("RetryPriorityWgt", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("MaxRequests", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("DBService", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("DBUser", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("DBPasswd", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("WaveServerConfig", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("WaveformPercentageThreshold", key) == 0 ) {
          return TN_SUCCESS;
        }
   	else if ( strcmp("StorageThreads", key) == 0 ) {
          return TN_SUCCESS;
        }
        else if ( strcmp("DBService", key) == 0 ) {
          return TN_SUCCESS;
        }
        else if ( strcmp("DBUser", key) == 0 ) {
          return TN_SUCCESS;
        }
        else if ( strcmp("DBPasswd", key) == 0 ) {
          return TN_SUCCESS;
        }
        else if ( strcmp("DBConnectionRetryInterval", key) == 0 ) {
          return TN_SUCCESS;
        }
        else if ( strcmp("DBMaxConnectionRetries", key) == 0 ) {
          return TN_SUCCESS;
        }

        return TN_FAILURE;
}

//! Initialize the instance 
int WaveArchiverPropertiesST::init(string config_filename) throw(Error){
    //Check if file exists//
    fstream file;
    file.open(config_filename.c_str(),fstream::in);
    if(file.fail()){
	throw Error("WaveArchiverPropertiesST::init() Unable to open "+config_filename);
    } 
    file.close();
 
    //Populate KeyValue Map//
    char key[MAXSTR];
    char value[MAXSTR];
    int ret=TN_EOF; 
    Configuration cfg(config_filename.c_str());
   
    cout << "WaveArchiverPropertiesST::init() BEGIN key=value list:" << '\n';
    while((ret=cfg.next(key,value))==TN_SUCCESS){
        if (strlen(key) == 1) {
           continue;
        }
	if(kvm[key] != ""){
	    //throw Error("WaveArchiverPropertiesST::init() Duplicate key found :"+string(key));
	    cout << "WaveArchiverPropertiesST::init() Duplicate key found:" << string(key) << '\n';
	}
        if ( strcmp(key,"DBPasswd") == 0 ) {
            cout << "    " << key << " = " << "XXXXXX" << '\n';
        }
        else {
            cout << "    " << key << " = " << value << '\n';
        }
        if ( checkPropertyKey(key) == TN_FAILURE ) {
	        cout << "WaveArchiverPropertiesST::init() Error, cfg file has unknown property key: '" << key << "'" << '\n';
 		return TN_FAILURE;
        }
	kvm[key] = value;
    }
    //Check for non-defaults//
    
   	if(kvm["Logfile"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter Logfile not found.");
				
    	}
	else{
		try{
			
					_logfile = kvm["Logfile"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. Logfile is invalid");
		}
	    }
   	
   	if(kvm["LoggingLevel"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter LoggingLevel not found.");
				
    	}
	else{
		try{
			
					_logginglevel = toInt(kvm["LoggingLevel"]);
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. LoggingLevel is invalid");
		}
	    }
   	
   	if(kvm["ProgramName"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter ProgramName not found.");
				
    	}
	else{
		try{
			
					_programname = kvm["ProgramName"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. ProgramName is invalid");
		}
	    }
   	
   	if(kvm["RequestType"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter RequestType not found.");
				
    	}
	else{
		try{
			
					_requesttype = kvm["RequestType"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. RequestType is invalid");
		}
	    }
   	
   	if(kvm["OutputDir"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter OutputDir not found.");
				
    	}
	else{
		try{
			
					_outputdir = kvm["OutputDir"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. OutputDir is invalid");
		}
	}
   	
   	if(kvm["ArchiveName"] == ""){
		_archivename = "SCEDC";	
				
    	}
	else{
		try{
			
					_archivename = kvm["ArchiveName"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. ArchiveName is invalid");
		}
	}
   	
   	if(kvm["RequestSource"] == ""){
			_requestSrc = "";	
    	}
	else{
		try{
			
					_requestSrc =  kvm["RequestSource"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. RequestSource is invalid");
		}
	}

   	if(kvm["Network"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter Network not found.");
				
    	}
	else{
		try{
			
					_network = kvm["Network"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. Network is invalid");
		}
	    }
   	
   	if(kvm["Source"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter Source not found.");
				
    	}
	else{
		try{
			
					_source = kvm["Source"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. Source is invalid");
		}
	    }
   	
   	if(kvm["MaxValidTime"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter MaxValidTime not found.");
				
    	}
	else{
		try{
			
					_maxvalidtime = toInt(kvm["MaxValidTime"]);
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. MaxValidTime is invalid");
		}
	    }
   	
   	if(kvm["PollInterval"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter PollInterval not found.");
				
    	}
	else{
		try{
			
					_pollinterval = toInt(kvm["PollInterval"]);
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. PollInterval is invalid");
		}
	    }
   	
   	if(kvm["Order"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter Order not found.");
				
    	}
	else{
		try{
			
					_order = kvm["Order"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. Order is invalid");
		}
	}
   	

   	if(kvm["RetryMin"] == ""){
		_retryMin = 0;
        }
        else {
		try{
			_retryMin = toInt(kvm["RetryMin"]);
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. RetryMin is not valid integer");
		}
        }
   	
   	if(kvm["RetryMax"] == ""){
		_retryMax = 9999;
        }
        else {
		try{
			_retryMax = toInt(kvm["RetryMax"]);
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. RetryMax is not valid integer");
		}
        }
   	
   	if(kvm["RetryPowerBaseSecs"] == ""){
		_retryPowerBaseSecs = 4;
        }
        else {
		try{
			_retryPowerBaseSecs = toInt(kvm["RetryPowerBaseSecs"]);
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. RetryPowerBaseSecs is not valid integer");
		}
        }
   	
   	if(kvm["RetryPriorityWgt"] == ""){
		_retryPriorityWgt = 0.;
        }
        else {
		try{
			_retryPriorityWgt = toDouble(kvm["RetryPriorityWgt"]);
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. RetryPriorityWgt is not valid double");
		}
        }
   	
   	if(kvm["MaxPacketLatencySecs"] == ""){
		
		//throw Error("WaveArchiverPropertiesST::init() Parameter MaxPacketLatencySecs not found.");
		_maxPacketLatencySecs = 60.;
				
    	}
	else{
		try{
			
			_maxPacketLatencySecs = toInt(kvm["MaxPacketLatencySecs"]);
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. MaxPacketLatencySecs is invalid");
		}
	    }
   	
   	if(kvm["MaxRequests"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter MaxRequests not found.");
				
    	}
	else{
		try{
			
					_maxrequests = toInt(kvm["MaxRequests"]);
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. MaxRequests is invalid");
		}
	    }
   	
   	if(kvm["DBService"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter DBService not found.");
				
    	}
	else{
		try{
			
					_dbservice = kvm["DBService"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. DBService is invalid");
		}
	    }
   	
   	if(kvm["DBUser"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter DBUser not found.");
				
    	}
	else{
		try{
			
					_dbuser = kvm["DBUser"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. DBUser is invalid");
		}
	    }
   	
   	if(kvm["DBPasswd"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter DBPasswd not found.");
				
    	}
	else{
		try{
			
					_dbpasswd = kvm["DBPasswd"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. DBPasswd is invalid");
		}
	    }
   	
   	if(kvm["WaveServerConfig"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter WaveServerConfig not found.");
				
    	}
	else{
		try{
			
					_waveserverconfig = kvm["WaveServerConfig"];
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. WaveServerConfig is invalid");
		}
	    }
   	
   	if(kvm["WaveformPercentageThreshold"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter WaveformPercentageThreshold not found.");
				
    	}
	else{
		try{
			
					_waveformpercentagethreshold = toInt(kvm["WaveformPercentageThreshold"]);
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. WaveformPercentageThreshold is invalid");
		}
	    }
   	
   	if(kvm["StorageThreads"] == ""){
		
			throw Error("WaveArchiverPropertiesST::init() Parameter StorageThreads not found.");
				
    	}
	else{
		try{
			
					_storagethreads = toInt(kvm["StorageThreads"]);
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. StorageThreads is invalid");
		}
	}

   	if(kvm["DBConnectionRetryInterval"] == ""){
		
				_dbConnRetryInterval = 60;
    	}
	else{
		try{
				_dbConnRetryInterval = toInt(kvm["DBConnectionRetryInterval"]);
					
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. DBConnectionRetryInterval is invalid");
		}
	}

   	if(kvm["DBMaxConnectionRetries"] == ""){
				_dbConnMaxRetries = 1;
    	}
	else{
		try{
				_dbConnMaxRetries = toInt(kvm["DBMaxConnectionRetries"]);
		}
		catch(Error& e){
			throw Error("WaveArchiverPropertiesST::init() Configuration file invalid format. DBMaxConnectionRetries is invalid");
		}
	}

        cout << "WaveArchiverPropertiesST::init() END key=value list:" << '\n';
             cout << "WaveArchiverPropertiesST::init() Configuration after parse: " << '\n';
             cout << "    _logfile="<< _logfile << '\n';
	     cout << "    _logginglevel="<< _logginglevel << '\n';
	     cout << "    _programname="<< _programname << '\n';
             cout << "    _dbConnMaxRetries="<< _dbConnMaxRetries << '\n';
             cout << "    _dbConnRetryInterval="<< _dbConnRetryInterval << '\n';
	     cout << "    _archivename="<< _archivename << '\n';
	     cout << "    _requesttype="<< _requesttype << '\n';
	     cout << "    _outputdir="<< _outputdir << '\n';
	     cout << "    _requestSrc="<< _requestSrc << '\n';
	     cout << "    _network="<< _network << '\n';
	     cout << "    _source="<< _source << '\n';
	     cout << "    _maxvalidtime="<< _maxvalidtime << '\n';
	     cout << "    _pollinterval="<< _pollinterval << '\n';
	     cout << "    _order="<< _order << '\n';
	     cout << "    _maxPacketLatencySecs="<< _maxPacketLatencySecs << '\n';
	     cout << "    _maxrequests="<< _maxrequests << '\n';
	     cout << "    _dbservice="<< _dbservice << '\n';
	     cout << "    _dbuser="<< _dbuser << '\n';
	     cout << "    _dbpasswd="<< _dbpasswd << '\n';
	     cout << "    _waveserverconfig="<< _waveserverconfig << '\n';
	     cout << "    _waveformpercentagethreshold="<< _waveformpercentagethreshold << '\n';
	     cout << "    _storagethreads="<< _storagethreads << '\n';
             cout << "    _retryMin="<< _retryMin << '\n';
             cout << "    _retryMax="<< _retryMax << '\n';
             cout << "    _retryPowerBaseSecs="<< _retryPowerBaseSecs << '\n';
             cout << "    _retryPriorityWgt="<< _retryPriorityWgt << '\n';
             cout << endl;

             return TN_SUCCESS;

}

//! Return integer value of configuration.
int WaveArchiverPropertiesST::toInt(string int_val) throw(Error){
        char val[32];
	strcpy(val,int_val.c_str());

	int v = atoi(val);
// 	if(errno == ERANGE || errno == EINVAL){
// 		throw Error("Invalid Integer");
// 	}
	
	return v;
}

//! Return float value of configuration.
float WaveArchiverPropertiesST::toFloat(string float_val) throw(Error){
        char val[32];
	strcpy(val,float_val.c_str());

	float v = atof(val);
// 	if(errno == ERANGE || errno == EINVAL){
// 		throw Error("Invalid Float");
// 	}
	return v;
}

//! Return double value of configuration. 
double WaveArchiverPropertiesST::toDouble(string double_val) throw(Error){
        char val[32];
	strcpy(val,double_val.c_str());

	double v = (double)atof(val);
// 	if(errno == ERANGE || errno == EINVAL){
// 		throw Error("Invalid Double");
// 	}
	return v;
}

//! Return true or false for boolean configuration. 
bool WaveArchiverPropertiesST::toBool(string bool_val) throw(Error){
	if(bool_val == "true"){
		return true; 
	}
	else if(bool_val == "false"){
		return false;
	}
	else{
		throw Error("Invalid Bool");
	}
}


/// Return log file name 
string WaveArchiverPropertiesST::getLogfile() const{
	return _logfile;
}

/// Return logging level.
int WaveArchiverPropertiesST::getLoggingLevel() const{ /**< Return logging level. 0=ERROR only, 1=ERROR+INFO, 2=ERROR+INFO+DEBUG, in any such in source */ 
	return _logginglevel;
}

/// Return program instance name.
string WaveArchiverPropertiesST::getProgramName() const{
	return _programname;
}

/// Return request type, one of Triggered or Continuous.
string WaveArchiverPropertiesST::getRequestType() const{
	return _requesttype;
}

/// Return directory name where waveform output is written.
string WaveArchiverPropertiesST::getOutputDir() const{
	return _outputdir;
}

/// Return archive name.
string WaveArchiverPropertiesST::getArchiveName() const{
	return _archivename;
}

/// Return network ID of operating network.
string WaveArchiverPropertiesST::getNetwork() const{
	return _network;
}

/// Return source Id of waveforms.
string WaveArchiverPropertiesST::getSource() const{
	return _source;
}

/// Return maximum number of days a request card is considered valid.
int WaveArchiverPropertiesST::getMaxValidTime() const{
	return _maxvalidtime;
}

/// Return interval of time, in seconds, to wait between polls of the database for new request cards.
int WaveArchiverPropertiesST::getPollInterval() const{
	return _pollinterval;
}

/// Return minimum request_card.retry value to process, defaults to 0 when not declared.
int WaveArchiverPropertiesST::getRetryMin() const{
	return _retryMin;
}

/// wavearchiver ignores requests whose retry value is greater than RetryMax.
int WaveArchiverPropertiesST::getRetryMax() const{
	return _retryMax;
}

/// Return RetryPowerBaseSecs. See configuration section of manpage of details
int WaveArchiverPropertiesST::getRetryPowerBaseSecs() const{
	return _retryPowerBaseSecs;
}

/// Return RetryPriorityWgt. See configuration section of manpage for details.
double WaveArchiverPropertiesST::getRetryPriorityWgt() const{
	return _retryPriorityWgt;
}

/// Return order in which requests should be processed, LIFO or FIFO.
string WaveArchiverPropertiesST::getOrder() const{
	return _order;
}

/// Return source Id of requests.
string WaveArchiverPropertiesST::getRequestSource() const{
	return _requestSrc;
}

/// Return longest time needed to fill a packet for the slowest one or at least the vast majority of channels
int WaveArchiverPropertiesST::getMaxPacketLatencySecs() const{
	return _maxPacketLatencySecs;
}

/// Return maximum number of request cards to be read from database at a time.
int WaveArchiverPropertiesST::getMaxRequests() const{
	return _maxrequests;
}

/// Database service Id
string WaveArchiverPropertiesST::getDBService() const{
	return _dbservice;
}

/// Database user.
string WaveArchiverPropertiesST::getDBUser() const{
	return _dbuser;
}

/// Database password.
string WaveArchiverPropertiesST::getDBPasswd() const{
	return _dbpasswd;
}

/// Return number of times to retry connecting to the database. 
int   WaveArchiverPropertiesST::getDBMaxConnectionRetries() const { 
	return _dbConnMaxRetries;
}

/// Return the number of seconds to wait before retrying database connection.
int   WaveArchiverPropertiesST::getDBConnectionRetryInterval() const {
	return _dbConnRetryInterval;
}

/// File name of waveserver config.
string WaveArchiverPropertiesST::getWaveServerConfig() const{
	return _waveserverconfig;
}

/// Percentage of the requested time period that the retrieved waveform must meet to be considered "complete".
int WaveArchiverPropertiesST::getWaveformPercentageThreshold() const{
	return _waveformpercentagethreshold;
}

/// Number of threads for writing waveforms
int WaveArchiverPropertiesST::getStorageThreads() const{
	return _storagethreads;
}

