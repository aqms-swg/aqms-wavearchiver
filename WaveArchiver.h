/** @file                                                                                                            
* @ingroup group_wavearchiver                                                                                        
* @brief Header for Wavearchiver.C                                                                                     
*/
/***********************************************************/
/*
* File Name :
*	WaveArchiver.h
*
* Original Author:
*	Patrick Small
*
*Description:
*
*
*Creation Date:
*	17 June 1999
*
*Modification History:
*	03 Mar 2017 - ADG: Updated for 64-bit cyctem compliance
*
*Usage Notes:
*/
/**********************************************************/

#ifndef wave_archiver_H
#define wave_archiver_H
// Various include files
#include <list>
#include <pthread.h>
#include <libconfig.h++>
#include <stdint.h>
#include "GenLimits.h"
#include "DatabaseLimits.h"
#include "StatusManager.h"
#include "Logfile.h"
#include "Client.h"
#include "DatabaseWA.h"
#include "WaveClient.h"
#include "RequestCard.h"
#include "RequestInfo.h"
#include "LockedQueue.h"
#include "WaveArchiverPropertiesST.h"

using namespace libconfig;
using namespace std;

/// Enum for status of a request card
enum RCStatus{FAILED,SUCCESS,NODATA,UNKNOWN,PENDING,OVERFLEW} ;
//enum RCStatus{FAILED,SUCCESS,NODATA,UNKNOWN,PENDING} ;

/// Type definition of a wave server list
// Typedefinition for a wave server list
typedef vector<Client> WaveServerList;

class RCSession;

/// List of request card sessions.
typedef list< RCSession* >  RCSessionList;
//typedef LockedQueue< RCSession* > RCSessionQueue;

/// Encapsulates a queue containing request cards.
class RCSessionQueue : public LockedQueue< RCSession* > {
 public:
  int queue_size;
  string name;
};

/// Map of wave server group and associated queue.
typedef map< string, RCSessionQueue* > RSQGroupMap;
/// List of request card sessions.
typedef list< RCSessionQueue* > RCSessionQueueList;
typedef map < string, RCSessionQueueList* > RSQListMap;
typedef list< pthread_t > ThreadList;


/// Encapsulate request card session.
class RCSession {
 public:
    RequestCard rc; 
    RequestInfo rcinfo;
    Waveform wave;
    RCStatus status;
    //    void *next_rsq; //points to 
    RCSessionQueueList::iterator next_rsq;
    bool init;
    TimeStamp arrival_time;

    RCSession();
    ~RCSession();
};


/// Class for thread fulfilling a request card queue
class RCThread {
 public:
    //    Client server;
  RCSessionQueue *rsq;
  WaveClient wc;
  pthread_t thread;
  int tnum;
 public:
    RCThread();
};

/// List of request card threads
typedef list<RCThread*> RCThreadList;


/// Wave archiver class
class WaveArchiver
{

 private:    
    static WaveArchiverPropertiesST *prop;
    Config srvcfg;
    requestType reqmode;
    char outputdir[MAXSTR];
    char archivename[DB_MAX_ARCHIVE_LEN+1];
    char requestSrc[32];
    retrievalOrder order;
    static int servercount;
    unsigned int pcthreshold;
    StatusManager sm;
    static TimeStamp perf_start_time,perf_end_time;
    static int perf_rc_count;
    static pthread_mutex_t db_lock;

    pthread_t strgthread;
    static bool shutdown_requested;
    static bool stop_storage_thread;

    //New variables for Parallel Processing//
    static RCSessionQueue processedq;
    //static RCSessionQueue sinkq;
    static RCThreadList rcthreadlist;
    static ThreadList  storage_threads;
    // static pthread_t  sink_thread;

    static RSQGroupMap rsqgroupmap;
    static RSQListMap  rsq_selector_map;


    int _DumpOldRequests(RequestList &rl);
    int _RequestCardFeeder();
    int _GetRequests(RequestList &rl);
    int _ProcessRequests(RequestList &rl);
    int _SaveWaveform(uint32_t evid, const RequestCard &rc,
		      WaveformArchive &wa, Waveform &wave);


    static int _dispatchRCSession(RCSession*);
    int _setupArchiverThreads();
    static void* _archiver_thread(void*);
    static void* _storage_thread(void*);
    // static void* _sink_thread(void*);

 public:
    WaveArchiver(); 
    ~WaveArchiver();

    int ParseConfiguration(string filename); /*< Parse configuration files */
    int Startup(); /*!< Initialize data structures, setup threads, connect to dataabase, etc */
    int Run(); /*!< run wave archiver */
};



#endif

