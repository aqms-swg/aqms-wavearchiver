/** @file                                                                                                                                                               
* @ingroup group_wavearchiver                                                                                                                                           
* @brief Queue with mutex lock for push and pop
*/
#ifndef __lockedqueue_h
#define __lockedqueue_h

#include <pthread.h>
#include <queue>
#include <map>

enum QUEUE_ACCESS_MODE {BLOCKED,UNBLOCKED};

template <class T> 
class LockedQueue {

 private:
  queue<T> _queue;
  pthread_mutex_t _qlock;   
  pthread_cond_t  _qcond; 
  bool mywaitflag;
  

 public:
  LockedQueue(){
    pthread_mutex_init(&_qlock,NULL);
    pthread_cond_init(&_qcond,NULL);   
    mywaitflag = 0;
  }

  ~LockedQueue(){
  }

  int getSize(){
    int size = 0;
    //cout <<" Waiting for lock in getSize(): thread : "<<pthread_self()<<endl;
    pthread_mutex_lock(&_qlock);
    size = _queue.size();
    pthread_mutex_unlock(&_qlock);     
    //cout <<" Unlocked in getSize(): thread :"<<pthread_self()<<endl;
    return size;
  }

  int push(T e,QUEUE_ACCESS_MODE mode){
    switch (mode){
    case BLOCKED:
      //    //cout <<" Waiting for lock in push(): thread : "<<pthread_self()<<endl;
      pthread_mutex_lock(&_qlock);
      _queue.push(e);
      if(mywaitflag==1){
	  mywaitflag = 0;
	  //cout <<"Signaling waiting threads."<<endl;
	  pthread_cond_broadcast(&_qcond);
      }
      pthread_mutex_unlock(&_qlock); 
      return 0;

    case UNBLOCKED:
      int errcode = pthread_mutex_trylock(&_qlock);
      if(errcode==0){
	_queue.push(e);	
	if(mywaitflag==1){
	  mywaitflag = 0;
	  pthread_cond_broadcast(&_qcond);
	}
	pthread_mutex_unlock(&_qlock); 
      }
      else{
	return -1;
      }
      return 0;
    };
  }
  
  int pop(T& e,QUEUE_ACCESS_MODE mode){
    switch (mode){
    case BLOCKED:
    //cout <<" Waiting for lock in pop(BLOCKED): thread : "<<pthread_self()<<endl;
      pthread_mutex_lock(&_qlock);
      if(_queue.empty()){
	mywaitflag = 1; 
	//cout <<" Waiting in cond_wait in pop(BLOCKED): thread : "<<pthread_self()<<endl;
	struct timespec   ts;
	struct timeval    tp;

	gettimeofday(&tp, NULL);
	/* Convert from timeval to timespec */
	ts.tv_sec  = tp.tv_sec;
	ts.tv_nsec = tp.tv_usec * 1000;
	ts.tv_sec += 5;
	
	if(pthread_cond_timedwait(&_qcond,&_qlock,&ts)!=0){
	  pthread_mutex_unlock(&_qlock);
	  return -1;
	}
	//cout <<" Got signal in pop(BLOCKED): thread : "<<pthread_self()<<endl;
      }

      if(_queue.size() > 0){
	e = _queue.front();
	_queue.pop();
      }
      else{
	pthread_mutex_unlock(&_qlock);
	return -1;
      }
      pthread_mutex_unlock(&_qlock);
      //cout <<" unlocked in pop(BLOCKED): thread : "<<pthread_self()<<endl;
      return 0;
      
    case UNBLOCKED:
      int errcode = pthread_mutex_trylock(&_qlock);
      if(errcode == 0){
	if(_queue.empty()){
	  mywaitflag = 1; 
	  //cout <<" Waiting in cond_wait in pop(UNBLOCKED): thread : "<<pthread_self()<<endl;
	  // //cout <<" Thread waiting "<<endl;
	  pthread_cond_wait(&_qcond,&_qlock);
	  //cout <<" Got signal in pop(UNBLOCKED): thread : "<<pthread_self()<<endl;
	}
	e = _queue.front();
	_queue.pop();
	pthread_mutex_unlock(&_qlock);
	return 0;
      }
      else{
	return -1;
      }
    };
  }
  
};


#endif
