/** @file                                                                                                            
* @ingroup group_wavearchiver                                                                                        
* @brief Exceptions related to wave archiver                                                                                      
*/
#include "Exceptions.h"
#include <cstring>
#include <cstdio>
#include <cstdlib>

using namespace std;

Error::Error(){}

Error::Error(const char* str){
    addMessage((char*)str);
}

Error::Error(string str){
    addMessage((char*)str.c_str());
}

Error::~Error(){
    for(StringList::iterator it=msgstack.begin();it!=msgstack.end();it++){
	char* s = (*it);
	if(s)
	    free(s);
    }
    msgstack.clear();
}

void Error::addMessage(char* msg){
    if(!msg)
	return;
    char* s = strdup(msg); 
    if(!s)
	return;

    msgstack.push_back(s);
}

void Error::print(void){
    
    int c=0;
    for(StringList::iterator it=msgstack.begin();it!=msgstack.end();it++){
	for(int i=0;i<c;i++){
	    char s[5] = " ";
	    printf("\t%s",s);
	}
	c++;
	char* str = *it;
	printf("%s\n",str);
    }
}

LogicalError::LogicalError(){}
LogicalError::LogicalError(char* str) : Error(str){}

CriticalError::CriticalError(){}
CriticalError::CriticalError(char* str) : Error(str){}

OutofMemoryError::OutofMemoryError(){}

DatabaseError::DatabaseError(): LogicalError(){}
DatabaseError::DatabaseError(char* str): LogicalError(str){}

FileFormatError::FileFormatError(){}
FileFormatError::FileFormatError(char* str): LogicalError(str){}

FileIOError::FileIOError(){}
FileIOError::FileIOError(char* str): LogicalError(str){}

