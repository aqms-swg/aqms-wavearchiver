/** @file
 * @ingroup group_wavearchiver
 * @brief Get unique thread id
 */

#include <sys/types.h> 
pid_t gettid(void); 

#include <stdio.h> 
#include <linux/unistd.h> 
#include <sys/syscall.h> 
#include <unistd.h> 
inline
pid_t gettid( void ) 
{ 
        return syscall( __NR_gettid ); 
}
